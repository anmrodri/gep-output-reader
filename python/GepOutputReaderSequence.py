# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
# from AthenaCommon.Include import include

#
# GepOutputReader was designed to read clusters and jets produced by the
# TrigL0GepPerf package. Note that the same algorithms should be run
# by the TrigL0GepPerf package for the requested collections to be available.
# Only 'CaloCal', 'Calo420', and 'Calo422' topoclusters are provided by default.
# Default available options:
#
#  Topoclusters: topoclAlgs = ['CaloCal','Calo420','Calo422']
#  Pileup suppression: puSupprAlgs = ['', 'Vor', 'SK', 'VorSK']
#  Jet reconstruction: jetAlgs = ['AntiKt4']
#
# Jet containers present in the original AOD can also be retrieved
# by passing the container names to the list aodJetContainers.
#

def setupGepOutputReaderCfg(flags,
    produceNtuples,
    produceCaloCellsMap,
    topoclAlgs = ['Calo422'],
    puSupprAlgs = [''],
    jetAlgs = ['AntiKt4'], # This contains GEPJets + StandardJets
    emAlgs = [],
    tauAlgs = [],
    aodJetContainers=['InTimeAntiKt4TruthJets'], # Separated AODJets
    getCellsInfo = True,
    getEventInfo = True,
    getTruthInfo = True,
    getJetConstituentsInfo = True,
    getJetSeedsInfo = False,
    outputFileName = 'outputGEPNtuple.root'
    ):

    #Necessary to make the input cluster container name accurate with the clustering algorithms in Athena.

    caloclustercolls = {'Calo420' : 'CaloTopoClusters',
                        'Calo422' : 'CaloTopoClusters422',
                        'TopoTower' : 'CaloCalAllTopoTowers'}

    result = ComponentAccumulator()

    #from GepOutputReaderConf import WriteOutputTree
    #from GepOutputReaderConf import WriteOutputHist

    clusters = []
    gepjets = []

    # clusters and jets produced by TrigL0GepPerf
    for puSupAlg in puSupprAlgs:
        for topoMaker in topoclAlgs:
            caloClustersKey = caloclustercolls.get(topoMaker, None)
            if caloClustersKey is None:
                cl_name = "GEP"+topoMaker+puSupAlg+"Clusters"
            else:
                cl_name = caloClustersKey
            clusters.append( cl_name )
            for jetAlg in jetAlgs: # JJ: Create GEP jet branches
                if ('jFex' not in jetAlg) and ('AntiKt4Truth' not in jetAlg) and ('AntiKt4EMPFlow' not in jetAlg): # JJ: There can be a better boolean
                    jet_name = jetAlg + topoMaker+ 'Jets'

                    gepjets.append( jet_name )
    
    if "AntiKt4Truth" in jetAlgs: # JJ: One TruthJets collection only
        gepjets.append('AntiKt4TruthJets')
    if "AntiKt4EMPFlow" in jetAlgs: # JJ: One PFLowJets collection only
        gepjets.append('AntiKt4EMPFlowJets')

    # jets read directly from the original AOD
    aodjets = []
    for aodJet in aodJetContainers:
        aodjets.append( aodJet )

    jfexJets = []
    for jetAlg in jetAlgs:
        if ('L1_jFex' in jetAlg):
            jfexJets.append(jetAlg)

    efexEMs = []
    for emAlg in emAlgs:
        if ('L1_eEM' in emAlg): 
            efexEMs.append(emAlg)

    efexTaus = []
    for tauAlg in tauAlgs:
        if ('L1_eTau' in tauAlg or 'L1_cTau' in tauAlg): 
            efexTaus.append(tauAlg)

    OutputTreeAlg = CompFactory.WriteOutputTree('Write ntuples',
                                          ClustersList=clusters,
                                          GEPJetList=gepjets, # JJ: vector of strings
                                          AODJetList=aodjets,
                                          ClustersKeys=clusters, #ANA: Clearly not ideal but does the job for now
                                          GEPJetsKeys=gepjets, #ANA: Clearly not ideal but does the job for now
                                          AODJetsKeys=aodjets, # JJ: ReadHandleKeyArray
                                          GetEventInfo=getEventInfo,
                                          GetCellsInfo=getCellsInfo,
                                          GetTruthInfo=getTruthInfo,
                                          GetJetConstituentsInfo=getJetConstituentsInfo,
                                          GetJetSeedsInfo=getJetSeedsInfo,
                                          JFexJetList=jfexJets,JFexJetKeys=jfexJets,
                                          EFexEMList=efexEMs,EFexEMKeys=efexEMs,
                                          EFexTauList=efexTaus,EFexTauKeys=efexTaus)
    if(produceNtuples):
        result.addEventAlgo(OutputTreeAlg)

    #We need The hist service to output nutples
    histSvc = CompFactory.THistSvc(Output=["outputStream DATAFILE='"+outputFileName+"' OPT='RECREATE'"])
    result.addService(histSvc)

#adding the Cells Map from r21.9
    if(produceCaloCellsMap):
        CaloCellMapAlg = CompFactory.WriteOutputTreeCells('Write CaloCellsMap')
        result.addEventAlgo(CaloCellMapAlg)
        result.getService("THistSvc").Output += ["outputCalStream DATAFILE='CaloCells.root' OPT='RECREATE'"]

    return result
