#include "WriteOutputTree.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "JetEDM/PseudoJetVector.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloClusterAuxContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"

#include "xAODTrigger/jFexSRJetRoIContainer.h"
#include "xAODTrigger/jFexSRJetRoI.h"
#include "xAODTrigger/eFexEMRoIContainer.h"
#include "xAODTrigger/eFexEMRoI.h"
#include "xAODTrigger/eFexTauRoIContainer.h"
#include "xAODTrigger/eFexTauRoI.h"

using xAOD::Jet;
using jet::PseudoJetVector;

#include <string>


WriteOutputTree::WriteOutputTree( const std::string& name, ISvcLocator* pSvcLocator ) :
  AthAnalysisAlgorithm( name, pSvcLocator )
{
  //declareProperty("CaloNoiseTool",m_noiseTool,"Tool Handle for noise tool");
  declareProperty( "ClustersList", ClustList);
  declareProperty( "GEPJetList", GEPJetList);
  declareProperty( "AODJetList", AODJetList);
  declareProperty( "JFexJetList", JFexJetList);
  declareProperty( "EFexEMList", EFexEMList);
  declareProperty( "EFexTauList", EFexTauList);
  declareProperty( "ClustersKeys", m_inputClusterKeyArray);
  declareProperty( "GEPJetsKeys", m_inputGepJetKeyArray); // JJ: Separate GEPJets with AODJets
  declareProperty( "AODJetsKeys", m_inputAODJetKeyArray); // JJ: Separate GEPJets with AODJets
  declareProperty( "JFexJetKeys", m_jFexJetTobKeyList);
  declareProperty( "EFexEMKeys", m_eFexEMTobKeyList);
  declareProperty( "EFexTauKeys", m_eFexTauTobKeyList);
  declareProperty( "GetEventInfo", m_getEventInfo);
  declareProperty( "GetCellsInfo", m_getCellsInfo);
  declareProperty( "GetJetConstituentsInfo", m_getJetConstituentsInfo);
  declareProperty( "GetJetSeedsInfo", m_getJetSeedsInfo);
  declareProperty( "GetTruthInfo", m_getTruthInfo);
}

WriteOutputTree::~WriteOutputTree() {}


StatusCode WriteOutputTree::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  //convert input strings to TStrings
  for(unsigned int i=0; i<ClustList.size(); i++)m_cl_list.push_back(ClustList[i]);
  for(unsigned int j=0; j<GEPJetList.size(); j++)m_gep_jet_list.push_back(GEPJetList[j]);
  for(unsigned int j=0; j<AODJetList.size(); j++)m_aod_jet_list.push_back(AODJetList[j]);
  for(unsigned int j=0; j<JFexJetList.size(); j++)m_jfex_jet_list.push_back(JFexJetList[j]);
  for(unsigned int j=0; j<EFexEMList.size(); j++)m_efex_em_list.push_back(EFexEMList[j]);
  for(unsigned int j=0; j<EFexTauList.size(); j++)m_efex_em_list.push_back(EFexTauList[j]);

  //Handles need to be initialized!
  CHECK(m_eventInfo.initialize());
  CHECK(m_TruthParticles.initialize());
  CHECK(m_inputClusterKeyArray.initialize());
  CHECK(m_inputGepJetKeyArray.initialize());
  CHECK(m_inputAODJetKeyArray.initialize());
  CHECK(m_caloCell.initialize()); //ANA INCLUDED
  CHECK(m_jFexJetTobKeyList.initialize());
  CHECK(m_eFexEMTobKeyList.initialize());
  CHECK(m_eFexTauTobKeyList.initialize());

  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  //Define the tree
  m_tree = new TTree("ntuple","ntuple");

  // clusters
  for(unsigned int j=0; j<m_cl_list.size(); j++){
    m_cl_et[m_cl_list[j]] = {0.};
    m_cl_eta[m_cl_list[j]] = {0.};
    m_cl_phi[m_cl_list[j]] = {0.};
    m_cl_m[m_cl_list[j]] = {0.};
    m_cl_Ncells[m_cl_list[j]] = {0};
    m_cl_cell_ids[m_cl_list[j]] = {{0}};


    m_tree->Branch(m_cl_list[j]+"_et", &m_cl_et[m_cl_list[j]]);
    m_tree->Branch(m_cl_list[j]+"_eta", &m_cl_eta[m_cl_list[j]]);
    m_tree->Branch(m_cl_list[j]+"_phi", &m_cl_phi[m_cl_list[j]]);
    m_tree->Branch(m_cl_list[j]+"_m",   &m_cl_m[m_cl_list[j]]);
    m_tree->Branch(m_cl_list[j]+"_Ncells",  &m_cl_Ncells[m_cl_list[j]]);
    m_tree->Branch(m_cl_list[j]+"_cell_ids",  &m_cl_cell_ids[m_cl_list[j]]);

  }

  // GEP jets
  for(unsigned int j=0; j<m_gep_jet_list.size(); j++){
    m_gep_jet_pt[m_gep_jet_list[j]] = {0.};
    m_gep_jet_eta[m_gep_jet_list[j]] = {0.};
    m_gep_jet_phi[m_gep_jet_list[j]] = {0.};
    m_gep_jet_m[m_gep_jet_list[j]] = {0.};
    m_gep_jet_nConstituents[m_gep_jet_list[j]] = {0};

    m_tree->Branch(m_gep_jet_list[j]+"_pt", &m_gep_jet_pt[m_gep_jet_list[j]]);
    m_tree->Branch(m_gep_jet_list[j]+"_eta", &m_gep_jet_eta[m_gep_jet_list[j]]);
    m_tree->Branch(m_gep_jet_list[j]+"_phi", &m_gep_jet_phi[m_gep_jet_list[j]]);
    m_tree->Branch(m_gep_jet_list[j]+"_m",   &m_gep_jet_m[m_gep_jet_list[j]]);
    m_tree->Branch(m_gep_jet_list[j]+"_nConstituents",   &m_gep_jet_nConstituents[m_gep_jet_list[j]]);

    if (m_getJetConstituentsInfo)
    {
      m_gep_jetConst_et[m_gep_jet_list[j]] = {{0.}};
      m_gep_jetConst_eta[m_gep_jet_list[j]] = {{0.}};
      m_gep_jetConst_phi[m_gep_jet_list[j]] = {{0.}};

      m_tree->Branch(m_gep_jet_list[j] + "_constituentPt", &m_gep_jetConst_et[m_gep_jet_list[j]]);
      m_tree->Branch(m_gep_jet_list[j] + "_constituentEta", &m_gep_jetConst_eta[m_gep_jet_list[j]]);
      m_tree->Branch(m_gep_jet_list[j] + "_constituentPhi", &m_gep_jetConst_phi[m_gep_jet_list[j]]);
    }
    if (m_gep_jet_list[j].Contains("Cone"))
    {
      if (m_getJetSeedsInfo)
      {
        m_gep_jetSeed_et[m_gep_jet_list[j]] = {{0.}};
        m_gep_jetSeed_eta[m_gep_jet_list[j]] = {{0.}};
        m_gep_jetSeed_phi[m_gep_jet_list[j]] = {{0.}};

        m_tree->Branch(m_gep_jet_list[j] + "_seedPt", &m_gep_jetSeed_et[m_gep_jet_list[j]]);
        m_tree->Branch(m_gep_jet_list[j] + "_seedEta", &m_gep_jetSeed_eta[m_gep_jet_list[j]]);
        m_tree->Branch(m_gep_jet_list[j] + "_seedPhi", &m_gep_jetSeed_phi[m_gep_jet_list[j]]);
      }
    }
  }

  // AOD jets
  for(unsigned int j=0; j<m_aod_jet_list.size(); j++){
    m_aod_jet_pt[m_aod_jet_list[j]] = {0.};
    m_aod_jet_eta[m_aod_jet_list[j]] = {0.};
    m_aod_jet_phi[m_aod_jet_list[j]] = {0.};
    m_aod_jet_m[m_aod_jet_list[j]] = {0.};

    m_tree->Branch(m_aod_jet_list[j]+"_pt", &m_aod_jet_pt[m_aod_jet_list[j]]);
    m_tree->Branch(m_aod_jet_list[j]+"_eta", &m_aod_jet_eta[m_aod_jet_list[j]]);
    m_tree->Branch(m_aod_jet_list[j]+"_phi", &m_aod_jet_phi[m_aod_jet_list[j]]);
    m_tree->Branch(m_aod_jet_list[j]+"_m",   &m_aod_jet_m[m_aod_jet_list[j]]);
  }

  // jfex jets
  for(unsigned int j=0; j<m_jfex_jet_list.size(); j++){
    m_jfex_jet_et[m_jfex_jet_list[j]] = {0.};
    m_jfex_jet_eta[m_jfex_jet_list[j]] = {0.};
    m_jfex_jet_phi[m_jfex_jet_list[j]] = {0.};

    m_tree->Branch(m_jfex_jet_list[j]+"_et", &m_jfex_jet_et[m_jfex_jet_list[j]]);
    m_tree->Branch(m_jfex_jet_list[j]+"_eta", &m_jfex_jet_eta[m_jfex_jet_list[j]]);
    m_tree->Branch(m_jfex_jet_list[j]+"_phi", &m_jfex_jet_phi[m_jfex_jet_list[j]]);
  }

  // efex ems
  for(unsigned int j=0; j<m_efex_em_list.size(); j++){
    m_efex_em_et[m_efex_em_list[j]] = {0.};
    m_efex_em_eta[m_efex_em_list[j]] = {0.};
    m_efex_em_phi[m_efex_em_list[j]] = {0.};

    m_tree->Branch(m_efex_em_list[j]+"_et", &m_efex_em_et[m_efex_em_list[j]]);
    m_tree->Branch(m_efex_em_list[j]+"_eta", &m_efex_em_eta[m_efex_em_list[j]]);
    m_tree->Branch(m_efex_em_list[j]+"_phi", &m_efex_em_phi[m_efex_em_list[j]]);
  }

  // efex taus
  for(unsigned int j=0; j<m_efex_tau_list.size(); j++){
    m_efex_tau_et[m_efex_tau_list[j]] = {0.};
    m_efex_tau_eta[m_efex_tau_list[j]] = {0.};
    m_efex_tau_phi[m_efex_tau_list[j]] = {0.};

    m_tree->Branch(m_efex_tau_list[j]+"_et", &m_efex_tau_et[m_efex_tau_list[j]]);
    m_tree->Branch(m_efex_tau_list[j]+"_eta", &m_efex_tau_eta[m_efex_tau_list[j]]);
    m_tree->Branch(m_efex_tau_list[j]+"_phi", &m_efex_tau_phi[m_efex_tau_list[j]]);
  }

  if(m_getEventInfo){
    m_tree->Branch("eventNumber", &eventNumber);
    m_tree->Branch("runNumber", &runNumber);
    m_tree->Branch("weight", &weight);
    //m_tree->Branch("mcEventWeight", &mcEventWeights);
    m_tree->Branch("distFrontBunchTrain", &distFrontBunchTrain);
    // ANA: Can't get bcid
    // m_tree->Branch("BCID",&bcid);
    m_tree->Branch("averageInteractionsPerCrossing", &mu);
  }

  if(m_getCellsInfo){
    m_tree->Branch("cells_e", &cells_e);
    m_tree->Branch("cells_et", &cells_et);
    m_tree->Branch("cells_time", &cells_time);
    m_tree->Branch("cells_quality", &cells_quality);
    m_tree->Branch("cells_provenance", &cells_provenance);
    m_tree->Branch("cells_ID", &cells_ID);
  }
  if(m_getTruthInfo){
    m_tree->Branch("ph_parent" ,         &t_ph_parent);
    m_tree->Branch("ph_pt" ,             &t_ph_pt);
    m_tree->Branch("ph_eta" ,            &t_ph_eta);
    m_tree->Branch("ph_phi" ,            &t_ph_phi);

    m_tree->Branch("el_parent" ,         &t_el_parent);
    m_tree->Branch("el_pt" ,             &t_el_pt);
    m_tree->Branch("el_eta" ,            &t_el_eta);
    m_tree->Branch("el_phi" ,            &t_el_phi);

  }

  //Register it with the appropriate service
  CHECK(m_histSvc->regTree("/outputStream/myTree",m_tree));
  return StatusCode::SUCCESS;
}


StatusCode WriteOutputTree::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //

  return StatusCode::SUCCESS;
}


StatusCode WriteOutputTree::execute() {
  ATH_MSG_INFO ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed

  if(m_getEventInfo){
    CHECK(RetrieveEventInfo());
  }

  if(m_getCellsInfo){
    CHECK( RetrieveCells() );
  }

  if (m_getTruthInfo) {
	  CHECK (RetrieveTruth() );
  }

  if(!m_cl_list.empty()){
    CHECK( RetrieveClusters() );
  }
  if(!m_gep_jet_list.empty()){
    CHECK( RetrieveGEPJets() );
  }
  if(!m_aod_jet_list.empty()){
    CHECK( RetrieveAODJets() );
  }

  if(!m_jfex_jet_list.empty()){
     CHECK( RetrieveJFexJets() );
  }

  if(!m_efex_em_list.empty()){
     CHECK( RetrieveEFexEMs() );
  }
  if(!m_efex_tau_list.empty()){
     CHECK( RetrieveEFexTaus() );
  }

  //-------

  m_tree->Fill();

  setFilterPassed(true);
  return StatusCode::SUCCESS;
}

StatusCode WriteOutputTree::RetrieveEventInfo() {

  if(m_getEventInfo){
    mcEventWeights.clear();

    auto eventInfo = SG::makeHandle(m_eventInfo);
    CHECK(eventInfo.isValid());

    mu = eventInfo->averageInteractionsPerCrossing();
    eventNumber = eventInfo->eventNumber();
    runNumber = eventInfo->runNumber();
    //ANA: THis has been commented out because it does not handle it
    // ToolHandle<Trig::IBunchCrossingTool> m_bcTool("Trig::MCBunchCrossingTool/BunchCrossingTool");
    // distFrontBunchTrain = m_bcTool->distanceFromFront(eventInfo->bcid(), Trig::IBunchCrossingTool::BunchCrossings);
    // bcid = eventInfo->bcid();
    mcEventWeights = eventInfo->mcEventWeights();
    weight = eventInfo->mcEventWeights()[0];
  }

  return StatusCode::SUCCESS;
}


StatusCode WriteOutputTree::RetrieveCells() {

  if(m_getCellsInfo){
    cells_e.clear();
    cells_et.clear();
    cells_time.clear();
    cells_quality.clear();
    cells_provenance.clear();
    cells_ID.clear();

    // Cells info that changes between events
    // ANA OLD COMMENTED
    // const CaloCellContainer* cells = 0;
    // CHECK( evtStore()->retrieve( cells, "AllCalo") );

    auto cells = SG::makeHandle(m_caloCell);
    CHECK(cells.isValid());
    ATH_MSG_INFO(cells);

    for(auto cell : *cells) {
      float e          = cell->energy();
      float et         = cell->energy() * 1.0/TMath::CosH(cell->eta());
      float time       = cell->time();
      float quality    = cell->quality();
      float provenance = cell->provenance();
      unsigned cell_id = (cell->ID().get_identifier32()).get_compact();

      cells_e.push_back( e );
      cells_et.push_back( et );
      cells_time.push_back( time );
      cells_quality.push_back( quality );
      cells_provenance.push_back( provenance );
      cells_ID.push_back( cell_id );
    }
  }

  return StatusCode::SUCCESS;
}

StatusCode WriteOutputTree::RetrieveTruth() {
	// Truth particles
	auto m_TPs = SG::makeHandle(m_TruthParticles);
    CHECK(m_TPs.isValid());
    ATH_MSG_INFO(m_TPs);


	// clear objects
	//
    t_ph_parent.clear();
    t_ph_pt.clear();
    t_ph_eta.clear();
    t_ph_phi.clear();

    t_el_parent.clear();
    t_el_pt.clear();
    t_el_eta.clear();
    t_el_phi.clear();


	// Fill truth particles
	for (auto TP : *m_TPs){

			bool isPhoton = abs(TP->pdgId())==22;
			bool electronOrPositron = abs(TP->pdgId())==11;
			bool Zboson = abs(TP->pdgId())==23;
			if(Zboson) ATH_MSG_DEBUG("Z pT " << TP->pt() );
			bool correctStatus = TP->status()==1;
			if (correctStatus && TP->pt()>1e3){// ignore low pT e/gamma
				if (isPhoton && correctStatus){ 
					ATH_MSG_DEBUG ( "Is Photon");
					//ATH_MSG_DEBUG("Number of Photon parent " << TP->nParents() << " " << TP->parent()->pdgId());                                                                   
					// Cut out very low photon candidates. Also, places cut near reco threshold                                                                                      
					t_ph_pt.push_back(TP->pt());
					t_ph_eta.push_back(TP->eta());
					t_ph_phi.push_back(TP->phi());
					t_ph_parent.push_back(TP->parent()->pdgId());
				} else if (electronOrPositron){
					ATH_MSG_DEBUG ( "Is electron/positron");
					//ATH_MSG_DEBUG("Number of electron parent " << TP->nParents() << " " << TP->parent()->pdgId());                                                                   
					// Cut out very low photon candidates. Also, places cut near reco threshold                                                                                      
					t_el_pt.push_back(TP->pt());
					t_el_eta.push_back(TP->eta());
					t_el_phi.push_back(TP->phi());
					t_el_parent.push_back(TP->parent()->pdgId());

				} else {
					ATH_MSG_DEBUG ( "Not a photon nor electron. PGDID = " << TP->pdgId()); 
				}




			} else {
				ATH_MSG_DEBUG ( "Unstable particle, status = "<< TP->status() << " or low pT" << TP->pt() );
			}	
	}
	


	return StatusCode::SUCCESS;
}

StatusCode WriteOutputTree::RetrieveClusters() {

  size_t index = 0;

  for (const SG::ReadHandleKey<xAOD::CaloClusterContainer> & k : m_inputClusterKeyArray){

    auto inputClusterContainer = SG::makeHandle(k);
    CHECK(inputClusterContainer.isValid());

    const auto& cl_name = m_cl_list[index];

    ++index;

    m_cl_et[cl_name].clear();
    m_cl_eta[cl_name].clear();
    m_cl_phi[cl_name].clear();
    m_cl_m[cl_name].clear();
    m_cl_Ncells[cl_name].clear();
    m_cl_cell_ids[cl_name].clear();

    for(auto iCluster : *inputClusterContainer){
      m_cl_et[cl_name].push_back(iCluster->et());
      m_cl_eta[cl_name].push_back(iCluster->eta());
      m_cl_phi[cl_name].push_back(iCluster->phi());
      m_cl_m[cl_name].push_back( iCluster->m() );

      // Ncells
      // custom clusters don't have cells info
      if(cl_name.Contains("Calo")){
    	CaloClusterCellLink::const_iterator cellBegin = iCluster->cell_begin();
    	CaloClusterCellLink::const_iterator cellEnd = iCluster->cell_end();

    	int nCells = std::distance(cellBegin,cellEnd);
	std::vector<unsigned> ids;
	if (cl_name.Contains("422")) {
		for (auto cell_itr = iCluster->cell_begin(); cell_itr != iCluster->cell_end(); ++cell_itr) {
			ids.push_back((cell_itr->ID().get_identifier32()).get_compact());
		}
	}
    	m_cl_Ncells[cl_name].push_back( nCells );
	m_cl_cell_ids[cl_name].push_back(ids);
      }
    }
  }

  return StatusCode::SUCCESS;
}


StatusCode WriteOutputTree::RetrieveGEPJets() {

  size_t index = 0;

  for (const SG::ReadHandleKey<xAOD::JetContainer> & j : m_inputGepJetKeyArray){
    ATH_MSG_INFO ("Working on jet: " << j);

    auto inputJetContainer = SG::makeHandle(j);
    CHECK(inputJetContainer.isValid());

    const auto& jet_name = m_gep_jet_list[index];

    ++index;

    m_gep_jet_pt[jet_name].clear();
    m_gep_jet_eta[jet_name].clear();
    m_gep_jet_phi[jet_name].clear();
    m_gep_jet_m[jet_name].clear();
    m_gep_jet_nConstituents[jet_name].clear();

    if (m_getJetConstituentsInfo)
    {
      m_gep_jetConst_et[jet_name].clear();
      m_gep_jetConst_eta[jet_name].clear();
      m_gep_jetConst_phi[jet_name].clear();
    }

    if (m_getJetSeedsInfo)
    {
      m_gep_jetSeed_et[jet_name].clear();
      m_gep_jetSeed_eta[jet_name].clear();
      m_gep_jetSeed_phi[jet_name].clear();
    }

      for (auto iJet : *inputJetContainer)
      {
        m_gep_jet_pt[jet_name].push_back(iJet->pt());
        m_gep_jet_eta[jet_name].push_back(iJet->eta());
        m_gep_jet_phi[jet_name].push_back(iJet->phi());
        m_gep_jet_m[jet_name].push_back(iJet->m());
        m_gep_jet_nConstituents[jet_name].push_back(iJet->numConstituents());

        // Jet's constituents information
        // if this information was not provided in TrigL0GepPerf, the constituent vector will be empty
        if (m_getJetConstituentsInfo)
        {
          std::vector<float> constVec_et;
          std::vector<float> constVec_eta;
          std::vector<float> constVec_phi;

          const xAOD::JetConstituentVector constvec = iJet->getConstituents();
          for (xAOD::JetConstituentVector::iterator it = constvec.begin(); it != constvec.end(); it++)
          {
            const xAOD::CaloCluster *cl = static_cast<const xAOD::CaloCluster *>((*it)->rawConstituent());
            constVec_et.push_back(cl->et());
            constVec_eta.push_back(cl->eta());
            constVec_phi.push_back(cl->phi());
          }
          m_gep_jetConst_et[jet_name].push_back(constVec_et);
          m_gep_jetConst_eta[jet_name].push_back(constVec_eta);
          m_gep_jetConst_phi[jet_name].push_back(constVec_phi);
        }

        //Jet algorithm seeds information
        //if this information was not provided in TrigL0GepPerf or does not apply, the attributes are set to 0
        if (jet_name.Contains("Cone"))
        {
          if (m_getJetSeedsInfo)
          {
            m_gep_jetSeed_et[jet_name].push_back(iJet->getAttribute<float>("SeedEt"));
            m_gep_jetSeed_eta[jet_name].push_back(iJet->getAttribute<float>("SeedEta"));
            m_gep_jetSeed_phi[jet_name].push_back(iJet->getAttribute<float>("SeedPhi"));
          }
        }
      }
    }

  return StatusCode::SUCCESS;
}

StatusCode WriteOutputTree::RetrieveAODJets() {
  size_t index = 0;

  for (const SG::ReadHandleKey<xAOD::JetContainer> & j : m_inputAODJetKeyArray){
    ATH_MSG_INFO ("Working on jet: " << j);

    auto inputJetContainer = SG::makeHandle(j);
    CHECK(inputJetContainer.isValid());

    const auto& jet_name = m_aod_jet_list[index];

    ++index;

    m_aod_jet_pt[jet_name].clear();
    m_aod_jet_eta[jet_name].clear();
    m_aod_jet_phi[jet_name].clear();
    m_aod_jet_m[jet_name].clear();

    for (auto iJet : *inputJetContainer)
    {
      m_aod_jet_pt[jet_name].push_back(iJet->pt());
      m_aod_jet_eta[jet_name].push_back(iJet->eta());
      m_aod_jet_phi[jet_name].push_back(iJet->phi());
      m_aod_jet_m[jet_name].push_back(iJet->m());
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode WriteOutputTree::RetrieveJFexJets() {

  size_t index = 0;

  // Loop over jet read handle keys in key array
  for (const SG::ReadHandleKey<xAOD::jFexSRJetRoIContainer>& key : m_jFexJetTobKeyList){
    ATH_MSG_INFO ("Working on jFex jet: " << key);

    auto jFexContainer = SG::makeHandle(key);
    CHECK(jFexContainer.isValid());

    const auto& jfex_jet_name = m_jfex_jet_list[index];

    ++index;

    m_jfex_jet_et[jfex_jet_name].clear();
    m_jfex_jet_eta[jfex_jet_name].clear();
    m_jfex_jet_phi[jfex_jet_name].clear();

    const xAOD::jFexSRJetRoIContainer* jetDataContPtr = jFexContainer.cptr();
    for (const xAOD::jFexSRJetRoI *roi: *jetDataContPtr) {
        m_jfex_jet_et[jfex_jet_name].push_back(roi->et());
        m_jfex_jet_eta[jfex_jet_name].push_back(roi->eta());
        m_jfex_jet_phi[jfex_jet_name].push_back(roi->phi());
    }
  } // Finished jet loop

  return StatusCode::SUCCESS;
}

StatusCode WriteOutputTree::RetrieveEFexEMs() {

  size_t index = 0;

  // Loop over EM read handle keys in key array
  for (const SG::ReadHandleKey<xAOD::eFexEMRoIContainer>& key : m_eFexEMTobKeyList){
    ATH_MSG_INFO ("Working on eFex EM: " << key);

    auto eFexContainer = SG::makeHandle(key);
    CHECK(eFexContainer.isValid());

    const auto& efex_em_name = m_efex_em_list[index];

    ++index;

    m_efex_em_et[efex_em_name].clear();
    m_efex_em_eta[efex_em_name].clear();
    m_efex_em_phi[efex_em_name].clear();

    const xAOD::eFexEMRoIContainer* emDataContPtr = eFexContainer.cptr();
    for (const xAOD::eFexEMRoI *roi: *emDataContPtr) {
        m_efex_em_et[efex_em_name].push_back(roi->et());
        m_efex_em_eta[efex_em_name].push_back(roi->eta());
        m_efex_em_phi[efex_em_name].push_back(roi->phi());
    }
  } // Finished EM loop

  return StatusCode::SUCCESS;
}

StatusCode WriteOutputTree::RetrieveEFexTaus() {

  size_t index = 0;

  // Loop over Tau read handle keys in key array
  for (const SG::ReadHandleKey<xAOD::eFexTauRoIContainer>& key : m_eFexTauTobKeyList){
    ATH_MSG_INFO ("Working on eFex Tau: " << key);

    auto eFexContainer = SG::makeHandle(key);
    CHECK(eFexContainer.isValid());

    const auto& efex_tau_name = m_efex_tau_list[index];

    ++index;

    m_efex_tau_et[efex_tau_name].clear();
    m_efex_tau_eta[efex_tau_name].clear();
    m_efex_tau_phi[efex_tau_name].clear();

    const xAOD::eFexTauRoIContainer* tauDataContPtr = eFexContainer.cptr();
    for (const xAOD::eFexTauRoI *roi: *tauDataContPtr) {
        m_efex_tau_et[efex_tau_name].push_back(roi->et());
        m_efex_tau_eta[efex_tau_name].push_back(roi->eta());
        m_efex_tau_phi[efex_tau_name].push_back(roi->phi());
    }
  } // Finished Tau loop

  return StatusCode::SUCCESS;
}

