#ifndef WRITEOUTPUTTREE_WRITEOUTPUTTREE_H
#define WRITEOUTPUTTREE_WRITEOUTPUTTREE_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
//ANA:begin
#include "AthenaBaseComps/AthAlgorithm.h"
#include "Gaudi/Property.h"
#include "GaudiKernel/ToolHandle.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloClusterAuxContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODJet/JetContainer.h" //added this ANA
#include "xAODJet/JetAuxContainer.h"
#include "CaloEvent/CaloCompactCellContainer.h"
#include "JetEDM/PseudoJetVector.h" //added this ANA

#include "xAODTrigger/jFexSRJetRoIContainer.h"
#include "xAODTrigger/jFexSRJetRoI.h"
#include "xAODTrigger/eFexEMRoIContainer.h"
#include "xAODTrigger/eFexEMRoI.h"
#include "xAODTrigger/eFexTauRoIContainer.h"
#include "xAODTrigger/eFexTauRoI.h"

#include "TTree.h"
#include "GaudiKernel/ITHistSvc.h"
#include "StoreGate/ReadHandleKeyArray.h"
//ANA: end
#include "TTree.h"



class WriteOutputTree: public ::AthAnalysisAlgorithm {
 public:
  WriteOutputTree( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~WriteOutputTree();

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  //virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed

  virtual StatusCode RetrieveEventInfo();
  virtual StatusCode RetrieveCells();
  virtual StatusCode RetrieveClusters();
  virtual StatusCode RetrieveGEPJets();
  virtual StatusCode RetrieveTruth();
  virtual StatusCode RetrieveJFexJets();
  virtual StatusCode RetrieveEFexEMs();
  virtual StatusCode RetrieveEFexTaus();
  virtual StatusCode RetrieveAODJets();

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


 private:

   // //Service required for output of trees and histograms
   ServiceHandle<ITHistSvc> m_histSvc{this, "THistSvc", "THistSvc"};

   // //Handles to define collections to recover, these things are now configurable automatically !
   SG::ReadHandleKey<xAOD::EventInfo> m_eventInfo {this, "eventInfo", "EventInfo", "key to read in an EventInfo object"};
   SG::ReadHandleKeyArray<xAOD::CaloClusterContainer> m_inputClusterKeyArray;
   SG::ReadHandleKeyArray<xAOD::JetContainer> m_inputGepJetKeyArray;
   SG::ReadHandleKeyArray<xAOD::JetContainer> m_inputAODJetKeyArray;

  SG::ReadHandleKey<CaloCellContainer> m_caloCell {this, "AllCalo", "AllCalo", "key to read in an AllCalo object"}; // ANA: INCLUDE
  
  SG::ReadHandleKey<xAOD::TruthParticleContainer> m_TruthParticles {this, "TruthParticles", "TruthParticles", "key to read truth particles"}; 

  SG::ReadHandleKeyArray<xAOD::jFexSRJetRoIContainer> m_jFexJetTobKeyList;

  SG::ReadHandleKeyArray<xAOD::eFexEMRoIContainer> m_eFexEMTobKeyList;
  SG::ReadHandleKeyArray<xAOD::eFexTauRoIContainer> m_eFexTauTobKeyList;


  //Alg properties
  std::vector<std::string> ClustList;
  std::vector<std::string> GEPJetList; // JJ: Separate GEPJets with AODJets
  std::vector<std::string> AODJetList;
  std::vector<std::string> JFexJetList;
  std::vector<std::string> EFexEMList;
  std::vector<std::string> EFexTauList;
  bool m_getTruthInfo = true;
  bool m_getEventInfo = true;
  bool m_getCellsInfo = true;
  bool m_getJetConstituentsInfo = true;
  bool m_getJetSeedsInfo = false;
  std::vector<TString> m_cl_list;
  std::vector<TString> m_gep_jet_list;
  std::vector<TString> m_aod_jet_list;
  std::vector<TString> m_jfex_jet_list;
  std::vector<TString> m_efex_em_list;
  std::vector<TString> m_efex_tau_list;

  TTree* m_tree = nullptr;


  //Event info
  int distFrontBunchTrain;
  int bcid;
  int eventNumber;
  int runNumber;
  float weight;
  float mu;
  std::vector<float> mcEventWeights;


  //Cells
  std::vector<float> cells_e;
  std::vector<float> cells_et;
  std::vector<float> cells_time;
  std::vector<unsigned int> cells_quality;
  std::vector<unsigned int> cells_provenance;
  std::vector<unsigned> cells_ID;

  //Clusters
  std::map<TString, std::vector<float>> m_cl_et;
  std::map<TString, std::vector<float>> m_cl_eta;
  std::map<TString, std::vector<float>> m_cl_phi;
  std::map<TString, std::vector<float>> m_cl_m;
  std::map<TString, std::vector<int>> m_cl_Ncells;
  std::map<TString, std::vector<std::vector<unsigned>>> m_cl_cell_ids;

  //GEP Jets
  std::map<TString, std::vector<float>> m_gep_jet_pt;
  std::map<TString, std::vector<float>> m_gep_jet_phi;
  std::map<TString, std::vector<float>> m_gep_jet_eta;
  std::map<TString, std::vector<float>> m_gep_jet_m;
  std::map<TString, std::vector<int>> m_gep_jet_nConstituents;

  std::map<TString, std::vector<std::vector<float>>> m_gep_jetConst_et;
  std::map<TString, std::vector<std::vector<float>>> m_gep_jetConst_phi;
  std::map<TString, std::vector<std::vector<float>>> m_gep_jetConst_eta;
  std::map<TString, std::vector<float>> m_gep_jetSeed_et;
  std::map<TString, std::vector<float>> m_gep_jetSeed_phi;
  std::map<TString, std::vector<float>> m_gep_jetSeed_eta;

  //AOD Jets
  std::map<TString, std::vector<float>> m_aod_jet_pt;
  std::map<TString, std::vector<float>> m_aod_jet_phi;
  std::map<TString, std::vector<float>> m_aod_jet_eta;
  std::map<TString, std::vector<float>> m_aod_jet_m;

  //Truth
  std::vector<float> t_ph_parent;
  std::vector<float> t_ph_pt;
  std::vector<float> t_ph_eta;
  std::vector<float> t_ph_phi;

  std::vector<float> t_el_parent;
  std::vector<float> t_el_pt;
  std::vector<float> t_el_eta;
  std::vector<float> t_el_phi;

  //jFex Jets
  std::map<TString, std::vector<float>> m_jfex_jet_et;
  std::map<TString, std::vector<float>> m_jfex_jet_phi;
  std::map<TString, std::vector<float>> m_jfex_jet_eta;

  //eFex EMs
  std::map<TString, std::vector<float>> m_efex_em_et;
  std::map<TString, std::vector<float>> m_efex_em_phi;
  std::map<TString, std::vector<float>> m_efex_em_eta;

  //eFex Taus
  std::map<TString, std::vector<float>> m_efex_tau_et;
  std::map<TString, std::vector<float>> m_efex_tau_phi;
  std::map<TString, std::vector<float>> m_efex_tau_eta;
};

#endif //> !WRITEOUTPUTTREE_WRITEOUTPUTTREE_H
