#ifndef WRITEOUTPUTTREECELLS_WRITEOUTPUTTREECELLS_H
#define WRITEOUTPUTTREECELLS_WRITEOUTPUTTREECELLS_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

#include "AthenaBaseComps/AthAlgorithm.h"
#include "Gaudi/Property.h"
#include "GaudiKernel/ToolHandle.h"
#include "xAODEventInfo/EventInfo.h"
#include "CaloEvent/CaloCellContainer.h"
#include "CaloEvent/CaloCell.h"
#include "CaloDetDescr/CaloDetDescrManager.h"
#include "CaloIdentifier/CaloCell_ID.h"
#include "CaloConditions/CaloNoise.h"

#include "TTree.h"
#include "GaudiKernel/ITHistSvc.h"
#include "StoreGate/ReadHandleKeyArray.h"

class WriteOutputTreeCells: public ::AthAnalysisAlgorithm {
 public:
  WriteOutputTreeCells( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~WriteOutputTreeCells();

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  virtual StatusCode  finalize();       //once, after all events processed

  virtual StatusCode RetrieveCells();

 private:

   // //Service required for output of trees and histograms
   ServiceHandle<ITHistSvc> m_histSvc{this, "THistSvc", "THistSvc"};

   // //Handles to define collections to recover, these things are now configurable automatically !
   SG::ReadHandleKey<CaloCellContainer> m_caloCell {this, "AllCalo", "AllCalo", "key to read in an AllCalo object"};
   const CaloCell_ID* m_ccIdHelper;

   SG::ReadCondHandleKey<CaloNoise> m_electronicNoiseKey{this, "electronicNoiseKey", "totalNoise","SG Key of CaloNoise data object"};

   SG::ReadCondHandleKey<CaloNoise> m_totalNoiseKey{this, "totalNoiseKey", "totalNoise", "SG Key of CaloNoise data object"};

  // //Alg properties
  bool m_getCellsInfo = true;

  TTree* m_tree = nullptr;

  //Cells
  std::vector<float> cells_e;
  std::vector<float> cells_et;
  std::vector<float> cells_time;
  std::vector<unsigned int> cells_quality;
  std::vector<unsigned int> cells_provenance;

  std::vector<float> cells_eta;
  std::vector<float> cells_phi;
  std::vector<float> cells_sinTh;
  std::vector<float> cells_cosTh;
  std::vector<float> cells_cosPhi;
  std::vector<float> cells_sinPhi;
  std::vector<float> cells_cotTh;
  std::vector<float> cells_x;
  std::vector<float> cells_y;
  std::vector<float> cells_z;

  std::vector<float> cells_etaMin;
  std::vector<float> cells_etaMax;
  std::vector<float> cells_phiMin;
  std::vector<float> cells_phiMax;
  std::vector<float> cells_etaGranularity;
  std::vector<float> cells_phiGranularity;
  std::vector<int>   cells_layer;

  std::vector<unsigned int> cells_ID;
  std::vector<float> cells_totalNoise;
  std::vector<float> cells_electronicNoise;
  std::vector<std::string> cells_detName;
  std::vector<unsigned int> cells_sampling;
  std::vector<bool> cells_badcell;

  std::vector<bool> cells_IsEM;
  std::vector<bool> cells_IsEM_Barrel;
  std::vector<bool> cells_IsEM_EndCap;
  std::vector<bool> cells_IsEM_BarrelPos;
  std::vector<bool> cells_IsEM_BarrelNeg;
  std::vector<bool> cells_IsFCAL;
  std::vector<bool> cells_IsHEC;
  std::vector<bool> cells_IsTile;

  std::vector<std::vector<unsigned>> cells_neighbours;

};

#endif //> !WRITEOUTPUTTREECELLS_WRITEOUTPUTTREECELLS_H
