#include "WriteOutputTreeCells.h"

#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloClusterAuxContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"

#include <string>


WriteOutputTreeCells::WriteOutputTreeCells( const std::string& name, ISvcLocator* pSvcLocator ) :
  AthAnalysisAlgorithm( name, pSvcLocator )
{
}

WriteOutputTreeCells::~WriteOutputTreeCells() {}


StatusCode WriteOutputTreeCells::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  //Handles need to be initialized!
  CHECK(m_caloCell.initialize());
  CHECK(m_electronicNoiseKey.initialize());
  CHECK(m_totalNoiseKey.initialize());
  CHECK( detStore()->retrieve (m_ccIdHelper, "CaloCell_ID") );

  //Define the tree
  m_tree = new TTree("caloCellsMap","caloCellsMap");

  if(m_getCellsInfo){
    m_tree->Branch("cells_e", &cells_e);
    m_tree->Branch("cells_et", &cells_et);
    m_tree->Branch("cells_time", &cells_time);
    m_tree->Branch("cells_quality", &cells_quality);
    m_tree->Branch("cells_provenance", &cells_provenance);
  }

  m_tree->Branch("cells_eta",&cells_eta);
  m_tree->Branch("cells_phi",&cells_phi);
  m_tree->Branch("cells_sinTh",&cells_sinTh);
  m_tree->Branch("cells_cosTh",&cells_cosTh);
  m_tree->Branch("cells_sinPhi",&cells_sinPhi);
  m_tree->Branch("cells_cosPhi",&cells_cosPhi);
  m_tree->Branch("cells_cotTh",&cells_cotTh);
  m_tree->Branch("cells_x",&cells_x);
  m_tree->Branch("cells_y",&cells_y);
  m_tree->Branch("cells_z",&cells_z);

  m_tree->Branch("cells_etaMin",&cells_etaMin);
  m_tree->Branch("cells_etaMax",&cells_etaMax);
  m_tree->Branch("cells_phiMin",&cells_phiMin);
  m_tree->Branch("cells_phiMax",&cells_phiMax);
  m_tree->Branch("cells_etaGranularity",&cells_etaGranularity);
  m_tree->Branch("cells_phiGranularity",&cells_phiGranularity);
  m_tree->Branch("cells_layer",&cells_layer);

  m_tree->Branch("cells_ID",&cells_ID);

  m_tree->Branch("cells_totalNoise",&cells_totalNoise);
  m_tree->Branch("cells_electronicNoise",&cells_electronicNoise);
  m_tree->Branch("cells_detName",&cells_detName);
  m_tree->Branch("cells_sampling",&cells_sampling);
  m_tree->Branch("cells_badcell",&cells_badcell);

  m_tree->Branch("cells_IsEM", &cells_IsEM);
  m_tree->Branch("cells_IsEM_Barrel", &cells_IsEM_Barrel);
  m_tree->Branch("cells_IsEM_EndCap", &cells_IsEM_EndCap);
  m_tree->Branch("cells_IsEM_BarrelPos", &cells_IsEM_BarrelPos);
  m_tree->Branch("cells_IsFCAL", &cells_IsFCAL);
  m_tree->Branch("cells_IsHEC", &cells_IsHEC);
  m_tree->Branch("cells_IsTile", &cells_IsTile);

  m_tree->Branch("cells_neighbours",&cells_neighbours);

  //Register it with the appropriate service
  CHECK(m_histSvc->regTree("/outputCalStream/myTree",m_tree));
  return StatusCode::SUCCESS;
}


StatusCode WriteOutputTreeCells::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  return StatusCode::SUCCESS;
}

StatusCode WriteOutputTreeCells::execute() {

  return StatusCode::SUCCESS;
}


StatusCode WriteOutputTreeCells::firstExecute() {

  ATH_MSG_INFO ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed

    CHECK( RetrieveCells() );

  m_tree->Fill();

  setFilterPassed(true);
  return StatusCode::SUCCESS;
}

StatusCode WriteOutputTreeCells::RetrieveCells() {
    cells_e.clear();
    cells_et.clear();
    cells_time.clear();
    cells_quality.clear();
    cells_provenance.clear();
    cells_eta              .clear();
    cells_phi              .clear();
    cells_sinTh            .clear();
    cells_cosTh            .clear();
    cells_sinPhi           .clear();
    cells_cosPhi           .clear();
    cells_cotTh            .clear();
    cells_x                .clear();
    cells_y                .clear();
    cells_z                .clear();

    cells_etaGranularity   .clear();
    cells_phiGranularity   .clear();
    cells_etaMin           .clear();
    cells_etaMax           .clear();
    cells_phiMin           .clear();
    cells_phiMax           .clear();
    cells_layer            .clear();

    cells_ID               .clear();
    cells_totalNoise       .clear();
    cells_electronicNoise  .clear();
    cells_detName          .clear();
    cells_sampling         .clear();
    cells_badcell          .clear();

    cells_IsEM             .clear();
    cells_IsEM_Barrel      .clear();
    cells_IsEM_EndCap      .clear();
    cells_IsEM_BarrelPos   .clear();
    cells_IsFCAL           .clear();
    cells_IsHEC            .clear();
    cells_IsTile           .clear();

    cells_neighbours       .clear();

    auto cells = SG::makeHandle(m_caloCell);
    CHECK(cells.isValid());
    ATH_MSG_INFO(cells);

    SG::ReadCondHandle<CaloNoise> electronicNoiseHdl{m_electronicNoiseKey}; //,  ctx};
    if (!electronicNoiseHdl.isValid()) {return StatusCode::FAILURE;}
    const CaloNoise* electronicNoiseCDO = *electronicNoiseHdl;

    SG::ReadCondHandle<CaloNoise> totalNoiseHdl{m_totalNoiseKey}; //, ctx};
    if (!totalNoiseHdl.isValid()) {return StatusCode::FAILURE;}
    const CaloNoise* totalNoiseCDO = *totalNoiseHdl;

    for(auto cell : *cells) {
      float e          = cell->energy();
      float et         = cell->energy() * 1.0/TMath::CosH(cell->eta());
      float time       = cell->time();
      float quality    = cell->quality();
      float provenance = cell->provenance();

      cells_e.push_back( e );
      cells_et.push_back( et );
      cells_time.push_back( time );
      cells_quality.push_back( quality );
      cells_provenance.push_back( provenance );

      float electronicNoise = electronicNoiseCDO->getNoise(cell->ID(), cell->gain());
      float totalNoise = totalNoiseCDO->getNoise(cell->ID(), cell->gain());

      float badcell  = cell->badcell();
      float eta    = cell->eta();
      float phi    = cell->phi();
      float sinTh  = cell->sinTh();
      float cosTh  = cell->cosTh();
      float sinPhi = cell->sinPhi();
      float cosPhi = cell->cosPhi();
      float cotTh  = cell->cotTh();
      float x = cell->x();
      float y = cell->y();
      float z = cell->z();

      unsigned int samplingEnum = m_ccIdHelper->calo_sample(cell->ID());

      bool IsEM = m_ccIdHelper->is_em(cell->ID());
      bool IsEM_Barrel=false;
      bool IsEM_EndCap=false;
      bool IsEM_BarrelPos=false;

      if(IsEM){
        IsEM_Barrel=m_ccIdHelper->is_em_barrel(cell->ID());
        if(IsEM_Barrel){
          if(m_ccIdHelper->pos_neg(cell->ID())>0) IsEM_BarrelPos=true;
        }
        IsEM_EndCap=m_ccIdHelper->is_em_endcap(cell->ID());
      }

      bool isFCAL = m_ccIdHelper->is_fcal(cell->ID());
      bool isHEC  = m_ccIdHelper->is_hec(cell->ID());
      bool isTile = m_ccIdHelper->is_tile(cell->ID());

      std::string detName = CaloSampling::getSamplingName(samplingEnum);

      // get all neighbouring cells
      std::vector<unsigned> neighbours;
      std::vector<IdentifierHash> cellNeighbours;
      IdentifierHash cellHashID = m_ccIdHelper->calo_cell_hash(cell->ID());
      m_ccIdHelper->get_neighbours(cellHashID,LArNeighbours::super3D,cellNeighbours);

      for (unsigned int i_neighbour = 0; i_neighbour < cellNeighbours.size(); ++i_neighbour) {
          const CaloCell* neighbour = cells->findCell(cellNeighbours[i_neighbour]);
          if (neighbour) neighbours.push_back((neighbour->ID().get_identifier32()).get_compact());
          else ATH_MSG_WARNING("Couldn't access neighbour #" << i_neighbour << " for cell ID " << (cell->ID().get_identifier32()).get_compact());
      }

      unsigned int id = (cell->ID().get_identifier32()).get_compact();

      const CaloDetDescriptor *elt = cell->caloDDE()->descriptor();
      int layer = cell->caloDDE()->getLayer();

      float deta = elt->deta();
      float dphi = elt->dphi();

      float etamin = eta - (0.5*deta);
      float etamax = eta + (0.5*deta);

      float phimin = phi - (0.5*dphi);
      float phimax = phi + (0.5*dphi);


      cells_eta              .push_back( eta );
      cells_phi              .push_back( phi );
      cells_sinTh            .push_back( sinTh );
      cells_cosTh            .push_back( cosTh );
      cells_sinPhi           .push_back( sinPhi );
      cells_cosPhi           .push_back( cosPhi );
      cells_cotTh            .push_back( cotTh );
      cells_x                .push_back( x );
      cells_y                .push_back( y );
      cells_z                .push_back( z );

      cells_etaMin           .push_back( etamin );
      cells_etaMax           .push_back( etamax );
      cells_phiMin           .push_back( phimin );
      cells_phiMax           .push_back( phimax );
      cells_etaGranularity   .push_back( deta );
      cells_phiGranularity   .push_back( dphi );
      cells_layer            .push_back( layer );

      cells_ID               .push_back( id );
      cells_totalNoise       .push_back( totalNoise);
      cells_electronicNoise  .push_back( electronicNoise );
      cells_detName          .push_back( detName );
      cells_sampling         .push_back( samplingEnum );
      cells_badcell          .push_back( badcell );

      cells_IsEM             .push_back( IsEM );
      cells_IsEM_Barrel      .push_back( IsEM_Barrel );
      cells_IsEM_EndCap      .push_back( IsEM_EndCap );
      cells_IsEM_BarrelPos   .push_back( IsEM_BarrelPos );
      cells_IsFCAL           .push_back( isFCAL );
      cells_IsHEC            .push_back( isHEC );
      cells_IsTile           .push_back( isTile );

      cells_neighbours       .push_back( neighbours );
    }


  return StatusCode::SUCCESS;
}
