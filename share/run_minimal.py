#
# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#

if __name__ == '__main__':
    import sys

    ##################################################
    # Add an argument parser
    ##################################################
    from AthenaCommon.Logging import logging
    local_log = logging.getLogger('run_gep')
    info = local_log.info
    error = local_log.error

    from AthenaConfiguration.AllConfigFlags import initConfigFlags
    from TrigValTools.TrigValSteering import Input

    flags = initConfigFlags()
    ifile = ("mc21_14TeV.500538.MGPy8EG_NNPDF30LO_hh_bbbb_vbf_l1cvv1cv1.simul.AOD.e8222_s4203_rUSER.root")
    flags.Input.Files = [ifile]


    flags.Exec.MaxEvents = 10
    flags.Concurrency.NumThreads = 1
    flags.Concurrency.NumConcurrentEvents = 1
    flags.Scheduler.ShowDataDeps = True
    flags.Scheduler.CheckDependencies = True
    flags.Scheduler.ShowDataFlow = True
    flags.Trigger.EDMVersion = 3

    flags.lock()
    flags.dump()

    ##################################################
    # Set up central services: Main + Input reading + L1Menu + Output writing
    ##################################################
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    acc = MainServicesCfg(flags)

    from AthenaConfiguration.Enums import Format
    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    acc.merge(PoolReadCfg(flags))

    # add the creation of standard 420 Topoclusters
    from CaloRec.CaloTopoClusterConfig import CaloTopoClusterCfg
    calo_acc420 = CaloTopoClusterCfg(flags)  # default clusters: 420
    acc.merge(calo_acc420)

    #Add the creation of 422 clusters
    from TrigGepPerf.Add422Config import Add422Cfg
    acc.merge(Add422Cfg(flags))

    from AthenaCommon.Constants import DEBUG
    gepAlgs_output_level = DEBUG
   
    #Calling JetClustering from TrigGepPerf
    from TrigGepPerf.GepJetAlgConfig import GepJetAlgCfg
    alg_name='GepCalo422ModAntiKtAlg'
    acc.merge(GepJetAlgCfg(
        flags,
        name=alg_name,
        jetAlgName='ModAntikT',
        caloClustersKey="CaloTopoClusters422",
        outputJetsKey='GEPCalo422ModAntiKtJets',
        OutputLevel=gepAlgs_output_level))

    info('\nGepJetAlg properties dump\n')
    info(str(acc.getEventAlgo(alg_name)._properties))

    #ANA: This is the part I have to work on. I added it to the GepOutputReaderSequence.py
    from GepOutputReader.GepOutputReaderSequence import setupGepOutputReaderCfg
    acc.merge(setupGepOutputReaderCfg(flags,
        produceHist=0,
        produceNtuples=1,
        topoclAlgs=['Calo422'],
        inputJetContainer='GEPCalo422ModAntiKtJets',
        jetAlgs=['ModAntiKt'],
        puSupprAlgs=[''],
        getCellsInfo=False,
        getEventInfo=True))

    with open("L1Sim.pkl", "wb") as f:
        acc.store(f)
        f.close()

    sc = acc.run()
