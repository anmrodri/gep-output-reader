#
# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#

if __name__ == '__main__':
    import sys, os
    import glob

    ##################################################
    # Add an argument parser
    ##################################################
    from AthenaCommon.Logging import logging
    local_log = logging.getLogger('run_gep')
    info = local_log.info
    error = local_log.error

    import argparse
    p = argparse.ArgumentParser()

    p.add_argument('-n', '--nevents',
                   metavar='N',
                   type=int,
                   default=10,
                   help='Number of events to process if --execute is used, default=%(default)s')

    p.add_argument('-c', '--clusterAlgs',
                        default='Calo420,Calo422,WFS,TopoTower',
                        help='commma separated list of stategies for GepClusterAlg: [WFS,Calo420,Calo422,TopoTower]')

    p.add_argument('-j', '--jetAlgs',
                        default='AntiKt4,ModAntikT',
                        help='commma separated list of stategies for GepJetAlg:[Cone,ModAntikT,AntiKt4,AntiKt4Truth,AntiKt4EMPFlow,L1_jFexSRJetRoI]')

    p.add_argument('-i', '--inputFiles',
                        type=str,default='',
                        help="list of files to run over")
    
    p.add_argument('-f', '--fexs',
                        default=['eFex','jFex'],
                        help="fexs to enable")
    
    p.add_argument('--ems',
                        default=['L1_eEMRoI'],
                        help="EM algos")
    
    p.add_argument('--taus',
                        default=['L1_eTauRoI'],
                        help="Tau algos")

    p.add_argument('-o', '--outputFile',
                        type=str,default='outputGEPNtuple.root',
                        help="Name of output file")

    args = p.parse_args()

    args.inputFiles = [s.strip() for s in args.inputFiles.split(",")]

    clusterAlgNames = [c for c in args.clusterAlgs.split(',') if c]
    jetAlgNames = [j for j in args.jetAlgs.split(',') if j]
    info('GEP clusterAlgs: ' + str(clusterAlgNames))
    info('GEP jetAlgs: ' + str(jetAlgNames))
    info('Print'+ str(args.inputFiles))

    ##################################################
    # Configure all the flags
    ##################################################
    from AthenaConfiguration.AllConfigFlags import initConfigFlags
    from TrigValTools.TrigValSteering import Input

    flags = initConfigFlags()

    flags.Input.Files = args.inputFiles if args.inputFiles != [''] else [
        "mc21_14TeV.800301.Py8EG_A14NNPDF23LO_flatpT_Zprime_tthad.recon.AOD.e8557_s4422_r16130/AOD.41930086._000020.pool.root.1"
            ]

    if not flags.Input.isMC:
        from AthenaConfiguration.TestDefaults import defaultGeometryTags
        flags.GeoModel.AtlasVersion = defaultGeometryTags.autoconfigure(flags)

    info('Command line args: ' + str(args))

    flags.Exec.MaxEvents = args.nevents
    flags.Concurrency.NumThreads = 1
    flags.Concurrency.NumConcurrentEvents = 1
    flags.Scheduler.ShowDataDeps = True
    flags.Scheduler.CheckDependencies = True
    flags.Scheduler.ShowDataFlow = True
    flags.Trigger.EDMVersion = 3

    flags.Trigger.L1.doCaloInputs = len(args.fexs)>0 # flag for saying if inputs should be decoded or not
    flags.Trigger.enableL1CaloPhase1 = len(args.fexs)>0 # used by this script to turn on/off the simulation
    # flags for rerunning simulation
    flags.Trigger.L1.doeFex = ('eFex' in args.fexs)
    flags.Trigger.L1.dojFex = ('jFex' in args.fexs)
    flags.Trigger.L1.dogFex = ('gFex' in args.fexs)
    flags.Trigger.triggerConfig='FILE'

    flags.lock()
    flags.dump()

    ##################################################
    # Set up central services: Main + Input reading + L1Menu + Output writing
    ##################################################
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    acc = MainServicesCfg(flags)

    from AthenaConfiguration.Enums import Format
    if flags.Input.Format == Format.POOL:
        from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
        acc.merge(PoolReadCfg(flags))
    else:
        from TriggerJobOpts.TriggerByteStreamConfig import ByteStreamReadCfg
        acc.merge(ByteStreamReadCfg(flags))

    if len(args.fexs)>0:
        # Generate L1 menu
        from TrigConfigSvc.TrigConfigSvcCfg import L1ConfigSvcCfg, generateL1Menu, createL1PrescalesFileFromMenu,getL1MenuFileName
        generateL1Menu(flags)
        createL1PrescalesFileFromMenu(flags)
        menuFilename = getL1MenuFileName(flags)
        if os.path.exists(menuFilename):
          print("Using L1Menu:",menuFilename)
        else:
          print("L1Menu file does not exist:",menuFilename)
          sys.exit(1)
        acc.merge(L1ConfigSvcCfg(flags))
    
        from L1CaloFEXSim.L1CaloFEXSimCfg import L1CaloFEXSimCfg
        acc.merge(L1CaloFEXSimCfg(flags))

    # add the creation of standard 420 Topoclusters
    from CaloRec.CaloTopoClusterConfig import CaloTopoClusterCfg
    calo_acc420 = CaloTopoClusterCfg(flags)  # default clusters: 420
    acc.merge(calo_acc420)

    # add the creation of 422 Topoclusters1
    from TrigGepPerf.Add422Config import Add422Cfg
    acc.merge(Add422Cfg(flags))

    from AthenaCommon.Constants import DEBUG
    gepAlgs_output_level = DEBUG

    ##################################################
    # GEP configuration
    ##################################################
    # Run clustering and jet finding algorithms
    # These may be produced by statndard ATLAS Algorithms, or by
    # GEP Algorithms.

    known_cluster_algs = ['WFS', 'Calo420', 'Calo422', 'TopoTower']
    for a in clusterAlgNames:
        info(str(a))
        assert a in known_cluster_algs
        info(str(a))

    known_gep_jet_algs = ['Cone', 'AntiKt4', 'ModAntikT', 'L1_jFexSRJetRoI']
    known_standard_jets = ['AntiKt4Truth', 'AntiKt4EMPFlow']
    gep_jet_algs = []
    standard_jets = []
    for a in jetAlgNames:
        if a in known_gep_jet_algs:
            gep_jet_algs.append(a)
        elif a in known_standard_jets:
            standard_jets.append(a)

    caloclustercolls = {'Calo420' : 'CaloTopoClusters',
                        'Calo422' : 'CaloTopoClusters422',
                        'TopoTower' : 'CaloCalAllTopoTowers'}

    for cluster_alg in clusterAlgNames:
        caloClustersKey = caloclustercolls.get(cluster_alg, None)
        if caloClustersKey is None:
            from TrigGepPerf.GepClusteringAlgConfig import GepClusteringAlgCfg

            caloClustersKey='GEP'+cluster_alg+'Clusters'

            # Defining the Energy encoding scheme used in GEP
            NumberOfEnergyBits = 6 # Two bits for energy range, rest to encode energy
            ValueOfLeastSignificantBit = 10 # in MeV
            ValueG = 4 # Multiplier for high energy ranges

            gepEnergyEncodingScheme = str(NumberOfEnergyBits)+"-"+str(ValueOfLeastSignificantBit)+"-"+str(ValueG)

            gepclustering_cfg = GepClusteringAlgCfg(
                flags,
                TopoClAlg=cluster_alg,
                outputCaloClustersKey=caloClustersKey,
                GEPEnergyEncodingScheme = gepEnergyEncodingScheme,
                HardwareStyleEnergyEncoding = True,
                TruncationOfOverflowingFEBs = True,
                OutputLevel=gepAlgs_output_level)

            info('Clusters: ' + str(clusterAlgNames))
            info('gepclustering_cfg dump:')
            gepclustering_cfg.printConfig(withDetails=True,
                                          summariseProps=True)

            acc.merge(gepclustering_cfg)

        puSuppressionAlgs = ['']

        for jetAlg in gep_jet_algs:
            if jetAlg=='AntiKt4':
                from TrigGepPerf.JetBuilder import modifyJets
                acc.merge(modifyJets(flags, cluster_alg, caloClustersKey))

            elif 'jFex' not in jetAlg:
                # run GEP jet algorithm
                from TrigGepPerf.GepJetAlgConfig import GepJetAlgCfg
                alg_name='Gep'+cluster_alg + jetAlg + 'JetAlg'
                acc.merge(GepJetAlgCfg(
                    flags,
                    name=alg_name,
                    jetAlgName=jetAlg,
                    caloClustersKey=caloClustersKey,
                    outputJetsKey=jetAlg + cluster_alg +'Jets',
                    OutputLevel=gepAlgs_output_level))

                info('\nGepJetAlg properties dump\n')
                info(str(acc.getEventAlgo(alg_name)._properties))

    for standard_jet in standard_jets:
        from TrigGepPerf.JetBuilder import GetStandardSmallRJets
        acc.merge(GetStandardSmallRJets(flags, standard_jet))
    
    info('Jets: ' + str(jetAlgNames))


    from GepOutputReader.GepOutputReaderSequence import setupGepOutputReaderCfg
    acc.merge(setupGepOutputReaderCfg(flags,
        produceNtuples=1,
        produceCaloCellsMap=1,
        topoclAlgs=clusterAlgNames,
        jetAlgs=jetAlgNames,
        emAlgs=args.ems,
        tauAlgs=args.taus,
        aodJetContainers=['InTimeAntiKt4TruthJets'], # JJ: Test
        puSupprAlgs=puSuppressionAlgs,
        getCellsInfo=True,
        getEventInfo=True,
        outputFileName=args.outputFile,
        ))

    ##################################################
    # Save and run the configuration
    ##################################################
    with open("L1Sim.pkl", "wb") as f:
        acc.store(f)
        f.close()

    sc = acc.run()
