#
# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaCommon.Logging import logging

def modifyJets(flags, cluster_alg, caloClustersKey):

    cfg = ComponentAccumulator()
    
    from JetRecConfig.JetRecConfig import JetRecCfg
    from JetRecConfig.StandardJetConstits import stdConstitDic as cst
    from ROOT import xAODType

    from JetRecConfig.JetDefinition import JetDefinition, JetInputConstitSeq

    cst[caloClustersKey] = JetInputConstitSeq(cluster_alg, xAODType.CaloCluster, ["EM"], caloClustersKey, caloClustersKey, jetinputtype="EMTopo")
    AntiKt4Algo = JetDefinition("AntiKt",0.4,cst[caloClustersKey],lock = True,)
    jetList = [AntiKt4Algo]

    for jd in jetList:
        cfg.merge(JetRecCfg(flags,jd))

    return cfg

def GetStandardSmallRJets(flags, jet_name):
    cfg = ComponentAccumulator()
    from JetRecConfig.JetRecConfig import JetRecCfg
    
    jetList = []

    if jet_name == 'AntiKt4Truth':
        from JetRecConfig.StandardSmallRJets import AntiKt4Truth
        truthJets = AntiKt4Truth.clone()
        truthJets.ghostdefs = ["Truth"]
        jetList.append(truthJets)
    elif jet_name == 'AntiKt4EMPFlow':
        from JetRecConfig.StandardSmallRJets import AntiKt4EMPFlow
        PFlowJets = AntiKt4EMPFlow.clone()
        jetList.append(PFlowJets)
    
    for jd in jetList: # Loop through standard jets
        cfg.merge(JetRecCfg(flags,jd))

    return cfg
